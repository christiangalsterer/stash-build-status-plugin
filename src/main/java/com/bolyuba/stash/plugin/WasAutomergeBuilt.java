package com.bolyuba.stash.plugin;

import com.atlassian.stash.build.BuildStatus;
import com.atlassian.stash.build.BuildStatusService;
import com.atlassian.stash.content.Changeset;
import com.atlassian.stash.scm.pull.MergeRequest;
import com.atlassian.stash.scm.pull.MergeRequestCheck;

import javax.annotation.Nonnull;

/**
 * @author Georgy Bolyuba (georgy@bolyuba.com)
 */
public class WasAutomergeBuilt implements MergeRequestCheck {

    private final AutomergeService automergeService;
    private final BuildStatusService buildStatusService;

    public WasAutomergeBuilt(AutomergeService automergeService, BuildStatusService buildStatusService) {
        this.automergeService = automergeService;
        this.buildStatusService = buildStatusService;
    }

    @Override
    public void check(@Nonnull MergeRequest mergeRequest) {
        if (!automergeService.shouldCheckMerge(mergeRequest.getPullRequest().getToRef().getRepository().getId())) {
            // feature was not enabled for this repository
            return;
        }

        Changeset automergeChangeset = automergeService.findAutomergeChangeset(mergeRequest.getPullRequest());
        if (automergeChangeset == null) {
            // cannot say anything in this case
            return;
        }

        BuildStatus lastStatus = automergeService.findLastStatus(automergeChangeset.getId());
        if (lastStatus == null) {
            mergeRequest.veto("Cannot merge", "No builds reported for automerge changeset [" + automergeChangeset.getId() + "].");
        } else {
            switch (lastStatus.getState()) {
                case INPROGRESS:
                    mergeRequest.veto("Cannot merge", "Current build of automerge changeset [" + automergeChangeset.getId() + "] is in progress. Check back later.");
                    break;
                case FAILED:
                    mergeRequest.veto("Cannot merge", "Last build of automerge changeset [" + automergeChangeset.getId() + "] failed.");
                    break;
            }
        }
    }
}
