package it.com.bolyuba.stash.plugin.helpers;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.webdriver.stash.page.FileBrowserPage;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * @author Georgy Bolyuba (georgy@bolyuba.com)
 */
public class MyFileBrowserPage extends FileBrowserPage {

    @ElementBy(cssSelector = ".fork-repo .aui-nav-item")
    private PageElement forkButton;

    public MyFileBrowserPage(String projectKey, String repoSlug) {
        super(projectKey, repoSlug);
    }

    public ForkRepoPage clickForkButton() {
        forkButton.click();

//        WebDriverWait wait = new WebDriverWait(driver, 10);
//        wait.until(ExpectedConditions.elementToBeClickable(By.id("fork-repo-submit")));

        return pageBinder.bind(ForkRepoPage.class, projectKey, slug);
    }
}
