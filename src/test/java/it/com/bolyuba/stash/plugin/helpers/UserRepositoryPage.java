package it.com.bolyuba.stash.plugin.helpers;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.webdriver.stash.page.UserProfilePage;
import org.openqa.selenium.By;

/**
 * @author Georgy Bolyuba (georgy@bolyuba.com)
 */
public class UserRepositoryPage extends UserProfilePage {

    protected String userSlug;

    protected String repoSlug;

    @ElementBy(cssSelector = ".create-pull-request .aui-nav-item")
    private PageElement createPullRequestButton;


    public UserRepositoryPage(String userSlug, String repoSlug) {
        super(userSlug);
        this.userSlug = userSlug;
        this.repoSlug = repoSlug;
    }

    @Override
    public String getUrl() {
        return String.format("%s/repos/%s/browse", super.getUrl(), repoSlug);
    }

    public String getHttpCloneUrl() {
        return driver.findElement(By.id("clone-repo-button")).getAttribute("href");
    }

    public UserRepositoryPullRequestCreatePage clickCreatePullRequest() {
        createPullRequestButton.click();
        return pageBinder.bind(UserRepositoryPullRequestCreatePage.class, userSlug, repoSlug);
    }

}
